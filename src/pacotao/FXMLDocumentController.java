package pacotao;


import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import pacotao.Pessoa;
import pacotao.Professor;
public class FXMLDocumentController implements Initializable {



    private ArrayList<Pessoa> listaP = new ArrayList();

    private ArrayList<Pessoa> listaA = new ArrayList();



    private Professor pr;

    private FuncAdm f;

    private Aluno a;



    private int i = 0;

    private int j;



    @FXML

    private Pane painel;



    @FXML

    private Button botaoDet, botDel, botAtua, enviar1, enviar2;



    @FXML

    private TextField txt1, txt2, txt3, txt4, txt5, txt6;



    @FXML

    private Label lblSalario, lblSemestre, lblCurso, lblSetor, lblFuncao, lblDisciplina;



    @FXML

    private RadioButton rbAluno, rbFunc, rbProf;



    @FXML

    private TableView<Pessoa> tabela;



    @FXML

    private TableColumn<Pessoa, String> nomeT;



    @FXML

    private TableColumn<Pessoa, Integer> idadeT;



    @FXML

    private TableColumn<Pessoa, String> enderecoT;



    @FXML

    private TableColumn<Pessoa, String> funcaoT;



    @FXML

    private TableColumn<Pessoa, String> semestreT;



    @FXML

    private TableColumn<Pessoa, String> cursoT;



    @FXML

    private TableColumn<Pessoa, Double> salarioT;



    @FXML

    private TableColumn<Pessoa, String> disciplinaT;



    @FXML

    private TableColumn<Pessoa, String> setorT;



    @FXML

    private TableColumn<Pessoa, String> funcao2T;



    @FXML

    final ToggleGroup group = new ToggleGroup();



    @Override

    public void initialize(URL url, ResourceBundle rb) {



        carregarDados();



        painel.setVisible(false);



        botaoDet.setVisible(false);

        botDel.setVisible(false);

        botAtua.setVisible(false);



        semestreT.setVisible(false);

        setorT.setVisible(false);

        disciplinaT.setVisible(false);

        salarioT.setVisible(false);

        cursoT.setVisible(false);

        funcaoT.setVisible(false);



        rbAluno.setToggleGroup(group);

        rbFunc.setToggleGroup(group);

        rbProf.setToggleGroup(group);



    }



    @FXML

    void showFormAluno() {

        painel.setVisible(true);



        txt6.setVisible(false);



        lblSemestre.setVisible(true);

        lblCurso.setVisible(true);

        lblDisciplina.setVisible(false);

        lblSalario.setVisible(false);

        lblSetor.setVisible(false);

        lblFuncao.setVisible(false);



        rbAluno.setVisible(false);

        rbProf.setVisible(false);

        rbFunc.setVisible(false);



        enviar1.setVisible(true);

        enviar2.setVisible(false);



    }



    @FXML

    void showFormFuncAdm() {

        painel.setVisible(true);



        txt6.setVisible(true);



        lblSalario.setVisible(true);

        lblSetor.setVisible(true);

        lblFuncao.setVisible(true);

        lblDisciplina.setVisible(false);

        lblCurso.setVisible(false);

        lblSemestre.setVisible(false);



        rbAluno.setVisible(false);

        rbProf.setVisible(false);

        rbFunc.setVisible(false);



        enviar1.setVisible(true);

        enviar2.setVisible(false);

    }



    @FXML

    void showFormProf() {

        painel.setVisible(true);



        txt6.setVisible(false);



        lblSalario.setVisible(true);

        lblDisciplina.setVisible(true);



        lblCurso.setVisible(false);

        lblSemestre.setVisible(false);

        lblSetor.setVisible(false);

        lblFuncao.setVisible(false);



        rbAluno.setVisible(false);

        rbProf.setVisible(false);

        rbFunc.setVisible(false);



        enviar1.setVisible(true);

        enviar2.setVisible(false);



    }



    @FXML

    void showBotao(MouseEvent event) {

        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {

            botaoDet.setVisible(true);

            botDel.setVisible(true);

            botAtua.setVisible(true);

        }

    }



    @FXML

    void criar(MouseEvent event) {



        if (lblDisciplina.isVisible()) {

            i = 3;

        } else if (lblCurso.isVisible()) {

            i = 1;

        } else if (lblSetor.isVisible()) {

            i = 2;

        }



        tabela.setItems(getListaDePessoas());



        lblSalario.setVisible(false);

        lblDisciplina.setVisible(false);

        lblCurso.setVisible(false);

        lblSemestre.setVisible(false);

        lblSetor.setVisible(false);

        lblFuncao.setVisible(false);



        txt6.setVisible(false);



        painel.setVisible(false);



        rbAluno.setVisible(true);

        rbProf.setVisible(true);

        rbFunc.setVisible(true);



    }



    @FXML

    void deletar(ActionEvent event) {



        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();

        tabela.getItems().remove(selectedItem);



        botDel.setVisible(false);

        botaoDet.setVisible(false);

        botAtua.setVisible(false);

        setorT.setVisible(false);

        salarioT.setVisible(false);

        disciplinaT.setVisible(false);

        cursoT.setVisible(false);

        semestreT.setVisible(false);

        funcaoT.setVisible(false);

    }



    @FXML

    void atualizar(ActionEvent event) {

        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();



        if ("Aluno".equals(selectedItem.getFuncao2())) {

            showFormAluno();

        }



        if ("Professor".equals(selectedItem.getFuncao2())) {

            showFormProf();

        }



        if ("FuncAdm".equals(selectedItem.getFuncao2())) {

            showFormFuncAdm();

        }



        enviar1.setVisible(false);

        enviar2.setVisible(true);



        botaoDet.setVisible(false);

        botDel.setVisible(false);

        botAtua.setVisible(false);



    }



    @FXML

    void atualizar2(MouseEvent event) {

        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();



        if ("Aluno".equals(selectedItem.getFuncao2())) {

            for (int x = 0; x < listaP.size(); x++) {

                if (listaP.get(x).getId() == selectedItem.getId()) {



                    Aluno a = new Aluno();

                    a.setId(selectedItem.getId());

                    a.setNome(txt1.getText());

                    a.setIdade(Integer.parseInt(txt2.getText()));

                    a.setEndereco(txt3.getText());

                    a.setCurso(txt5.getText());

                    a.setSemestre(txt4.getText());

                    a.setFuncao2("Aluno");



                    listaP.set(x, a);

                    selectedItem = listaP.get(x);



                }

            }

        } else if ("Professor".equals(selectedItem.getFuncao2())) {

            for (int x = 0; x < listaP.size(); x++) {

                if (listaP.get(x).getId() == selectedItem.getId()) {



                    Professor b = new Professor();

                    b.setId(selectedItem.getId());

                    b.setNome(txt1.getText());

                    b.setIdade(Integer.parseInt(txt2.getText()));

                    b.setEndereco(txt3.getText());

                    b.setSalario(Double.parseDouble(txt4.getText()));

                    b.setDisciplina(txt5.getText());

                    b.setFuncao2("Professor");



                    listaP.set(x, b);

                    selectedItem = listaP.get(x);



                }

            }

        } else if ("FuncAdm".equals(selectedItem.getFuncao2())) {

            for (int x = 0; x < listaP.size(); x++) {

                if (listaP.get(x).getId() == selectedItem.getId()) {



                    FuncAdm c = new FuncAdm();

                    c.setId(selectedItem.getId());

                    c.setNome(txt1.getText());

                    c.setIdade(Integer.parseInt(txt2.getText()));

                    c.setEndereco(txt3.getText());

                    c.setSalario(Double.parseDouble(txt4.getText()));

                    c.setSetor(txt5.getText());

                    c.setFuncao(txt6.getText());

                    c.setFuncao2("FuncAdm");



                    listaP.set(x, c);

                    selectedItem = listaP.get(x);



                }

            }

        }

        tabela.refresh();



        lblSalario.setVisible(false);

        lblDisciplina.setVisible(false);

        lblCurso.setVisible(false);

        lblSemestre.setVisible(false);

        lblSetor.setVisible(false);

        lblFuncao.setVisible(false);



        txt6.setVisible(false);



        painel.setVisible(false);



        rbAluno.setVisible(true);

        rbProf.setVisible(true);

        rbFunc.setVisible(true);

    }



    @FXML

    void detalhar(ActionEvent event) {

        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();



        if ("Aluno".equals(selectedItem.getFuncao2())) {

            cursoT.setVisible(true);

            semestreT.setVisible(true);

            setorT.setVisible(false);

            salarioT.setVisible(false);

            disciplinaT.setVisible(false);

            funcaoT.setVisible(false);

        }



        if ("Professor".equals(selectedItem.getFuncao2())) {

            salarioT.setVisible(true);

            disciplinaT.setVisible(true);

            setorT.setVisible(false);

            funcaoT.setVisible(false);

            cursoT.setVisible(false);

            semestreT.setVisible(false);



        }



        if ("FuncAdm".equals(selectedItem.getFuncao2())) {

            salarioT.setVisible(true);

            setorT.setVisible(true);

            funcaoT.setVisible(true);

            cursoT.setVisible(false);

            semestreT.setVisible(false);

            disciplinaT.setVisible(false);



        }

    }



    public void carregarDados() {

        nomeT.setCellValueFactory(new PropertyValueFactory<>("nome"));

        idadeT.setCellValueFactory(new PropertyValueFactory<>("idade"));

        enderecoT.setCellValueFactory(new PropertyValueFactory<>("endereco"));

        semestreT.setCellValueFactory(new PropertyValueFactory<>("semestre"));

        cursoT.setCellValueFactory(new PropertyValueFactory<>("curso"));

        disciplinaT.setCellValueFactory(new PropertyValueFactory<>("disciplina"));

        salarioT.setCellValueFactory(new PropertyValueFactory<>("salario"));

        funcaoT.setCellValueFactory(new PropertyValueFactory<>("funcao"));

        setorT.setCellValueFactory(new PropertyValueFactory<>("setor"));

        funcao2T.setCellValueFactory(new PropertyValueFactory<>("funcao2"));



        tabela.setItems(getListaDePessoas());

    }



    private ObservableList<Pessoa> getListaDePessoas() {



        switch (i) {

            case 0:



                listaP.add(new Aluno("Ultimo", "Bananas", j, "Soraka", "Runeterra", 3000, "Aluno"));

                j++;



                listaP.add(new Aluno("8º", "Pyromancia", j, "Annie", "Runeterra", 12, "Aluno"));

                j++;



                listaP.add(new Aluno("Ultimo", "Wuju", j, "Wukong", "Ionia", 20, "Aluno"));

                j++;



                listaP.add(new FuncAdm("B", "Lead designer", 10000.00, j, "Morello", "California", 30, "FuncAdm"));

                j++;



                listaP.add(new FuncAdm("C", "Lead designer", 15000.00, j, "Rylai", "California", 30, "FuncAdm"));

                j++;



                listaP.add(new FuncAdm("A", "Lead designer", 15000.00, j, "Liandry", "California", 30, "FuncAdm"));

                j++;



                listaP.add(new Professor("Pyromancia", 500.00, j, "Brand", "Runeterra", 300, "Professor"));

                j++;



                listaP.add(new Professor("Wuju", 0.0, j, "Master Yi", "Ionia", 40, "Professor"));

                j++;



                listaP.add(new Professor("Bananas", 3500.00, j, "Estrelas", "Céu", 300, "Professor"));

                break;



            case 1:



                j++;

                listaP.add(new Aluno(txt4.getText(), txt5.getText(), j, txt1.getText(), txt3.getText(), Integer.parseInt(txt2.getText()), "Aluno"));



                break;

            case 2:



                j++;

                listaP.add(new FuncAdm(txt5.getText(), txt6.getText(), Double.parseDouble(txt4.getText()), j, txt1.getText(), txt3.getText(), Integer.parseInt(txt2.getText()), "FuncAdm"));



                break;

            case 3:



                j++;

                listaP.add(new Professor(txt5.getText(), Double.parseDouble(txt4.getText()), j, txt1.getText(), txt3.getText(), Integer.parseInt(txt2.getText()), "Professor"));



        }



        return FXCollections.observableList(listaP);

    }



}

