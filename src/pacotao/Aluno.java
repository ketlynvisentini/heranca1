
import pacotao.Pessoa;

public class Aluno extends Pessoa {



    private String semestre;

    private String curso;



    public Aluno() {

    }



    public Aluno(String semestre, String curso, int id, String nome, String endereco, int idade, String funcao2) {

        super(id, nome, endereco, idade, funcao2);

        this.semestre = semestre;

        this.curso = curso;

    }



    public String getSemestre() {

        return semestre;

    }



    public void setSemestre(String semestre) {

        this.semestre = semestre;

    }



    public String getCurso() {

        return curso;

    }



    public void setCurso(String curso) {

        this.curso = curso;

    }



    @Override

    public void setFuncao2(String funcao2) {

        this.funcao2 = funcao2;

    }



}
