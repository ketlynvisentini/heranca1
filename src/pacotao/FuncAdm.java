
import pacotao.Funcionario;

public class FuncAdm extends Funcionario {



    private String setor;

    private String funcao;



    public FuncAdm() {

    }



    public FuncAdm(String setor, String funcao, double salario, int id, String nome, String endereco, int idade, String funcao2) {

        super(salario, id, nome, endereco, idade, funcao2);

        this.setor = setor;

        this.funcao = funcao;

    }



    public String getSetor() {

        return setor;

    }



    public void setSetor(String setor) {

        this.setor = setor;

    }



    public String getFuncao() {

        return funcao;

    }



    public void setFuncao(String funcao) {

        this.funcao = funcao;

    }



    @Override

    public void setFuncao2(String funcao2) {

        this.funcao2 = funcao2;

    }



}
