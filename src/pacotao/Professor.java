package pacotao;


import pacotao.Funcionario;


public class Professor extends Funcionario {



    private String disciplina;



    public Professor() {

    }



    public Professor(String disciplina, double salario, int id, String nome, String endereco, int idade, String funcao2) {

        super(salario, id, nome, endereco, idade, funcao2);

        this.disciplina = disciplina;

    }



    public String getDisciplina() {

        return disciplina;

    }



    public void setDisciplina(String disciplina) {

        this.disciplina = disciplina;

    }



    @Override

    public void setFuncao2(String funcao2) {

        this.funcao2 = funcao2;

    }



}