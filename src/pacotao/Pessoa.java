package pacotao;

public abstract class Pessoa {



    protected int id;



    protected String nome;



    protected String endereco;



    protected int idade;



    protected String funcao2;



    public Pessoa() {

    }



    public Pessoa(int id, String nome, String endereco, int idade, String funcao2) {

        this.id = id;

        this.nome = nome;

        this.endereco = endereco;

        this.idade = idade;

        this.funcao2 = funcao2;

    }



    public int getId() {

        return id;

    }



    public void setId(int id) {

        this.id = id;

    }



    public String getNome() {

        return nome;

    }



    public int getIdade() {

        return idade;

    }



    public String getEndereco() {

        return endereco;

    }



    public String getFuncao2() {

        return funcao2;

    }



    public void setNome(String nome) {

        this.nome = nome;

    }



    public void setEndereco(String endereco) {

        this.endereco = endereco;

    }



    public void setIdade(int idade) {

        this.idade = idade;

    }



    public void setFuncao2(String funcao2) {

        this.funcao2 = funcao2;

    }



}