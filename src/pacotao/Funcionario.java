
import pacotao.Pessoa;

public abstract class Funcionario extends Pessoa {



    protected double salario;

    public Funcionario(){}

    

    public Funcionario(double salario, int id, String nome, String endereco, int idade, String funcao2) {

        super(id, nome, endereco, idade, funcao2);

        this.salario = salario;

    }



    public double getSalario() {

        return salario;

    }



    public void setSalario(double salario) {

        this.salario = salario;

    }



}